from Levenshtein import distance as dst
import pandas as pd

dog_data_path = '20210103_hundenamen.csv'
dog_reference_string = 'Luca'


def find_dog_names(data_path, reference_string):
    data = pd.read_csv(data_path)

    column_name = data.columns[0]
    result_set = {data[column_name][index] for index in data.index if
                  (dst(data[column_name][index], reference_string) == 1)}

    return result_set


print(';'.join(find_dog_names(dog_data_path, dog_reference_string)))
