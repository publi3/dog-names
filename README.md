# dog-names 
# Find all names that have a Levenshtein distance of 1 to "Luca"

### prerequisites
1. Python >= 3.7
2. git


### clone repository
1. Open git bash in the directory where you want the cloned directory:
2. Clone the repository: 
   ```sh
    git clone https://gitlab.com/publi3/dog-names.git
   ```


### virtual environment creation
1. Open a shell in the svm-model folder

2. Create a virtual environment inside the basedir of the project
    ```sh
    python -m venv venv
    ```


### virtual environment activation
1. Open a shell in the svm-model folder
  
2. On Windows, run:
    ```sh
    venv\Scripts\activate.bat
    ```
3. On Unix or MacOS, run:
    ```sh
    source venv/bin/activate
    ```


### install dependencies
1. Install the required dependencies
    ```sh
    pip install -r requirements.txt
    ```

   
### run script
1. Open a shell in the svm-model folder
2. Activate the virtual environment
3. Run the dog-names script: 
   ```sh
    python dog-names.py
   ```

